#!/bin/python3
import discord

# import logging

# logger = logging.getLogger('discord')
# logger.setLevel(logging.ERROR) # for hardcore logging, use .DEBUG
# handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
# handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
# logger.addHandler(handler)

import asyncio

import commands.playlist as playlist
import commands.dieroll as dieroll
import commands.rusroulette as RR
import commands.hangman as hangman
import commands.roles as roles
import commands.admin as admin
import commands.reply as reply
import commands.stats as stats
import commands.math as math
import commands.votes as votes
import commands.reddit as reddit

import sys

from discord.ext import commands

guild_blacklist = ["71628177541177344"]
channel_whitelist = ["277834580503953409"]

bot = commands.Bot(
    command_prefix=commands.when_mentioned_or("!"), description="Brabo the CHATBOT :D"
)

bot.add_cog(
    playlist.Music(bot)
)  # removed music bot for a while to check brabo stability, does not appear to have affected anything
bot.add_cog(reply.ReplyCommands(bot))
bot.add_cog(dieroll.DierollCommands(bot))
bot.add_cog(RR.RussianRoulette(bot))
bot.add_cog(hangman.HangmanCommands(bot))
bot.add_cog(roles.Whosplaying(bot))
bot.add_cog(admin.AdminCommands(bot))
bot.add_cog(stats.StatsCommands(bot))
bot.add_cog(math.MathCommands(bot))
bot.add_cog(votes.Votes(bot))
redditBot = reddit.RedditScraper(bot)
bot.add_cog(redditBot)


@bot.event
async def on_ready():
    print("Logged in as:\n{0} (ID: {0.id})".format(bot.user))
    try:
        data = await bot.application_info()
    except Exception as e:
        print("Couldn't retrieve invite link. Error: {}".format(e))
    print(discord.utils.oauth_url(data.id))


@bot.event
async def on_message(message):
    # white/blacklist here:
    if (
        message.guild is not None
        and message.guild.id in guild_blacklist
        and message.channel.id not in channel_whitelist
    ):
        return
    await bot.process_commands(message)


def read_config(filename):
    with open(filename) as file:
        retdict = {}
        for line in file:
            line = line.strip()  # ignore whitespace before and after the string
            if line and line[0] != "#":  # not a comment line
                linesp = line.split("=")  # split on '='
                if len(linesp) > 1:  # ignore lines without '='
                    retdict[linesp[0].strip()] = "".join(
                        linesp[1:]
                    ).strip()  # key = val
        return retdict


def open_config():
    if len(sys.argv) > 1:  # there's a command-line argument
        try:
            return read_config(sys.argv[1])
        except Exception as e:  # fuck it, direct it over to the next layer
            raise
    else:  # no command-line args, read the regular config
        try:
            return read_config("config.ini")
        except Exception as e:  # fuck it, direct it over to the next layer
            print('File "config.ini" not found, did you forget an argument?')


if __name__ == "__main__":
    config = open_config()
    if config and "token" in config:
        redditBot.login(
            config.get("redditUsername",""),
            config.get("redditPassword",""),
            config.get("redditClient",""),
            config.get("redditToken",""),
        )
        bot.run(config["token"])

    else:
        print("Could not get a token from a config file, exiting.")
