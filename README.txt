Brabo the utility and chat bot for discord.

Developed for python 3.5+ by Joran Dox and Joeri Temmerman

Usage:
- Install https://github.com/Rapptz/discord.py
- Install Praw (python -m pip install praw)
- Install sqlite 3
- Import braboDB.sql into a new database names brabo.db
	("sqlite3 brabo.db < braboDB.sql" for ubuntu, ymmv)
- Paste your token into the bot's code
	(copy "defaultconfig.ini" to "config.ini" and paste your token there)
- Pastes brabos reddit tokens in "config.ini"
- Run Brabo.py with python 3.5+
