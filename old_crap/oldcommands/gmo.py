# coding=utf-8
import urllib


def load():

    return {u"gmo": gmo, u"gmi": gmi, u"gmj": gmj, u"gmk": gmk}


def gmo(msg, timestamp, sender):
    u"""responds with a good morning greeting"""

    return "Goedemorgen :D"


def gmi(msg, timestamp, sender):
    u"""responds with a good morning greeting"""

    return "Goedemiddag :D"


def gmj(msg, timestamp, sender):
    u"""responds with a good morning greeting"""

    return "おはよう ᕕ( ᐛ )ᕗ"


def gmk(msg, timestamp, sender):
    u"""responds with a good morning greeting"""

    return "안녕하세요 ^o^"
