# coding: UTF-8

"""
other commands
"""


def load():
    ret = {
        u"wauw": wauw,
        u"pls": pls,
        u"plz": pls,
        u"wat": wat,
        u"++": inc,
        u"--": dec,
    }


def inc():
    """doesn't really do much ... yet"""
    return "Thanks :)"


def dec():
    """doesn't really do much ... yet"""
    return "Sorry :("


def wat():
    """wot"""
    return "https://www.youtube.com/watch?v=PDXrXBsTFSE"


def wauw():
    """ur a doodoo lmao"""
    return "https://www.youtube.com/watch?v=5sGD-wK_Brc#t=103"


def pls():
    """pls"""
    return "STAHP"


def gmo():
    """you know what it does"""
    return "good morning :)"
