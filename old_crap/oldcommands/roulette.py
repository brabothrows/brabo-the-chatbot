import random


def load():
    ret = {
        u"roulette": roulette,
    }


def roulette(msg, timestamp, user):
    """usage: !roulette <money> <number/red/black>\ncurrently doesn't keep track of scores so go wild"""

    bet_amount = msg
    bet_amount = bet_amount.split(" ", 1)
    amount = int(bet_amount[0])
    bet = str(bet_amount[1])

    green_number = [0, "00"]
    red_number = [1, 3, 5, 7, 9, 19, 21, 23, 25, 27, 12, 14, 16, 18, 30, 32, 36]
    black_number = [
        2,
        4,
        6,
        8,
        10,
        20,
        22,
        24,
        26,
        28,
        34,
        11,
        13,
        15,
        17,
        29,
        31,
        33,
        35,
    ]

    double_00 = False
    if double_00:
        spin_the_wheel = random.randint(0, 37)
        if spin_the_wheel == 37:
            spin_the_wheel == "00"
    else:
        spin_the_wheel = random.randint(0, 36)

    win = False
    ret = "Error"  # if this doesn't get replaced, it's wrong
    if spin_the_wheel in green_number:
        ret = "Rolled %i, Green" % (spin_the_wheel)

    elif spin_the_wheel in red_number:
        ret = "Rolled %i, Red" % (spin_the_wheel)
        if bet.lower() == "red":
            money = 2 * amount
            ret += "\nYou win %i coins!" % (money)
            win = True

    elif spin_the_wheel in black_number:
        ret = "Rolled %i, Black" % (spin_the_wheel)
        if bet.lower() == "black":
            money = 2 * amount
            ret += "\nYou win %i coins!" % (money)
            win = True

    if bet == str(spin_the_wheel):
        money = 36 * amount
        ret += "\nYou win %i coins!" % (money)
        win = True

    if not win:
        ret += "\nYou lose %i coins." % (amount)
    print ret
    print "bet = " + bet
    print "bet_l = " + bet.lower()
    print "spin = " + str(spin_the_wheel)
    return ret
