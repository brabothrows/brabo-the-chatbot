"""
This file handles everything that has to do with the notes.
"""

import sqlite3
import time


def load():
    return {u"addme": adduser, u"alias": setalias, u"note": setnote}


def load_checks():
    return [testnotes]


def adduser(msg, timestamp, sender):
    """ask brabo to add your account to the database, so you can get messages and shit"""
    conn = sqlite3.connect("brabo.db")
    c = conn.cursor()
    # check if the user's already in the DB
    c.execute("""SELECT * FROM users WHERE username = :usr""", {"usr": sender.lower()})
    dbout = c.fetchone()
    if dbout:
        return "silly you, you're already in the database"
    else:
        c.execute(
            """ INSERT OR REPLACE INTO users VALUES (:usr, 0, 0)""",
            {"usr": sender.lower()},
        )
        c.execute(
            """ INSERT OR REPLACE INTO aliases VALUES (:usr, :usr)""",
            {"usr": sender.lower()},
        )
        conn.commit()
        conn.close()
        return "added " + sender + " to my list"

    # checks arg[0] is a known alias, and then sets arg[1] as an alias to the same user


def setalias(msg, timestamp, sender):
    """add an alias for a user\nalias [alias] [user]\nset alias [alias] for user [user], so you can use [alias] to adress him"""
    conn = sqlite3.connect("brabo.db")
    c = conn.cursor()
    inpt = msg.lstrip().split(" ")
    if inpt[1].lower() == "for":
        del inpt[1]

    c.execute(
        """SELECT user FROM aliases WHERE alias = :target""",
        {"target": inpt[1].lower()},
    )
    dbout = c.fetchone()
    if not dbout:
        return "i don't know " + inpt[1] + ", tell him/her to !addme first"
    c.execute(
        """INSERT OR REPLACE INTO aliases VALUES (:al, :user)""",
        {"al": inpt[0].lower(), "user": dbout[0]},
    )
    conn.commit()
    conn.close()
    return 'inserted alias "' + inpt[0] + '" for user "' + dbout[0] + '"'


def setnote(msg, timestamp, user):
    """leave a note for a user\n!note [target] [message]"""
    print ("\nadding note")
    conn = sqlite3.connect("brabo.db")
    c = conn.cursor()
    msg = msg.split(" ", 1)
    # check if msg[0] contains a valid user
    c.execute("""SELECT * FROM aliases WHERE alias = :user""", {"user": msg[0].lower()})
    dbout = c.fetchone()
    # dbout is something, we have a match
    if dbout:
        msg[0] = dbout[1]
        note = timestamp.strftime("%d-%m-%Y, %H:%M") + ": " + user + ":\n" + msg[1]
        print (note)
        # insert note into database
        c.execute(
            """INSERT INTO notes VALUES (:usr, :note)""",
            {"usr": msg[0].lower(), "note": note},
        )
        # set flag for new messages
        c.execute(
            """UPDATE users SET hasnote = 1 WHERE username = :usr""",
            {"usr": msg[0].lower()},
        )
        conn.commit()
        conn.close()
        return "done"

    else:
        return (
            "can't leave a message for " + msg[0] + ", add an alias for him/her first"
        )


def testnotes(msg, timestamp, user):
    conn = sqlite3.connect("brabo.db")
    c = conn.cursor()
    # request flag for new messages
    print "SELECT hasnote FROM users WHERE username = " + user
    c.execute(
        """SELECT hasnote FROM users WHERE username = :user""", {"user": user.lower()}
    )
    dbout = c.fetchall()
    print dbout
    conn.commit()
    conn.close()
    if dbout == [(0,)]:
        return ""
    elif dbout == [(1,)]:
        return getnotes(user)
    else:
        print "\ntestnotes error"


def getnotes(user):
    conn = sqlite3.connect("brabo.db")
    c = conn.cursor()
    # fetch notes

    try:
        c.execute("""SELECT * FROM notes WHERE user = :user""", {"user": user.lower()})
        dbout = c.fetchall()
        # delete notes
        c.execute("""DELETE FROM notes WHERE user = :user""", {"user": user.lower()})
        # set flag to zero
        c.execute(
            """UPDATE users SET hasnote = 0 WHERE username = :usr""",
            {"usr": user.lower()},
        )
        conn.commit()

    except Exception as e:
        print (e)

    conn.close()

    if len(dbout) == 0:
        return "There are no new notes for you."
    else:
        ret = user + ", you have notes: \n"
        for row in dbout:
            ret += row[1] + "\n"
        return ret
