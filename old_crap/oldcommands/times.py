import sqlite3
import time


def load():
    return {u"time": gettime}


def load_checks():
    return []


def gettime(msg, timestamp, user):
    """ """
    try:
        id = int(msg)
    except ValueError:
        id = 0

    conn = sqlite3.connect("brabo.db")
    c = conn.cursor()
    c.execute("""SELECT tstamp FROM time WHERE id = :id""", {"id": id})
    dbout = c.fetchall()

    if len(dbout) == 0:
        ret = "time " + str(id) + " hasn't been used yet, and has been set."

    else:
        elapsed = int(time.time()) - dbout[0][0]
        d = int(elapsed / 86400)
        elapsed = elapsed % 86400
        h = int(elapsed / 3600)
        elapsed = elapsed % 3600
        m = int(elapsed / 60)
        s = elapsed % 60
        ret = (
            "it's been "
            + str(d)
            + " days "
            + str(h)
            + " hours "
            + str(m)
            + " minutes and "
            + str(s)
            + " seconds since you asked me to remember id "
            + str(id)
            + "."
        )

    c.execute(
        """INSERT OR REPLACE INTO time VALUES (:id ,:timestamp)""",
        {"id": id, "timestamp": int(time.time())},
    )
    conn.commit()

    return ret
