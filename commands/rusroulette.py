import random
import discord
from discord.ext import commands


class roulettevars:
    def __init__(self):
        self.bulletid = random.randint(1, 6)
        self.click = 0


class RussianRoulette(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.vars = {}

    @commands.group(invoke_without_command=True)
    async def roulette(self, ctx):
        """Rusian roulette."""
        if ctx.message.channel.id not in self.vars:
            self.vars[ctx.message.channel.id] = roulettevars()

        self.vars[ctx.message.channel.id].bulletid -= 1
        if self.vars[ctx.message.channel.id].bulletid > 0:
            await ctx.send("*click*")
            self.vars[ctx.message.channel.id].click += 1
        else:
            await ctx.send("**BANG!**")
            await ctx.send("Reloading...")
            self.vars[ctx.message.channel.id] = roulettevars()
        if self.vars[ctx.message.channel.id].click >= 5:
            await ctx.send("Clicked 5 times, spinning the chamber...")
            self.vars[ctx.message.channel.id] = roulettevars()

    @commands.command()
    async def spin(self, ctx):
        """Spin the chamber!"""
        if ctx.message.channel.id not in self.vars:
            self.vars[ctx.message.channel.id] = roulettevars()
        await ctx.send("Spinning the chamber...")
        self.vars[ctx.message.channel.id] = roulettevars()

    @commands.command()
    async def reload(self, ctx):
        """Reload the gun and spin the chamber."""
        if ctx.message.channel.id not in self.vars:
            self.vars[ctx.message.channel.id] = roulettevars()
        await ctx.send("Reloading...")
        self.vars[ctx.message.channel.id] = roulettevars()
