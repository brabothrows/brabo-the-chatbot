import asyncio
from discord.ext import commands


class MathCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(no_pm=False)
    async def math(self, ctx, msg=None):
        """Compiles LaTeX"""

        # await ctx.send('ctx has: `' + str(dir(ctx)) + '`')
        # await ctx.send('ctx.message has: `' + str(dir(ctx.message)) + '`')
        await ctx.send("Compiling LaTeX, this might take a while")

        with open("my_image.png", "rb") as f:
            await client.send_file(ctx.message.channel, f)
