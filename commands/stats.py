import asyncio
import requests
import time
from discord.ext import commands
import sys
import commands.database as db


class StatsCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(no_pm=False)
    async def stats(self, ctx, msg=None):
        """replies with some stats"""

        # await ctx.send('ctx has: `' + str(dir(ctx)) + '`')
        # await ctx.send('ctx.message has: `' + str(dir(ctx.message)) + '`')
        print("ctx.message has: `" + str(dir(ctx.message)) + "`")

        async for message in self.bot.logs_from(ctx.message.channel, limit=5):
            DB = db.database()
            await ctx.send(DB.insertStatsLine(message))
