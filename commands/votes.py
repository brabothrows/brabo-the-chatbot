import discord
from discord.ext import commands
import random
import string
import commands.database as database
from pprint import pprint


class Votes(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.group()
    async def Kappa(self, ctx):
        await ctx.send(":Kappa:")

    # @commands.group(invoke_without_command=True)
    @commands.group()
    async def testunicode(self, ctx):
        await ctx.send("adding 😁 to your message")
        await ctx.message.add_reaction("😁")
        await ctx.send("added 😁 to your message")

    @commands.group()
    async def testallemojis(self, ctx):
        await ctx.send("adding ten emojis to your message")
        for i in random.choices(list(self.bot.emojis), k=10):
            await ctx.message.add_reaction(i)
        await ctx.send("added ten emojis to your message")

    @commands.group()
    async def vote(self, ctx):
        db = database.database()
        pprint(ctx.message.content)
        for i in ctx.message.content:
            if not db.isBlacklistChar(i):
                try:
                    print("trying to add " + i)
                    await ctx.message.add_reaction(i)
                except Exception as e:
                    print(type(e), e)
                    print(f"'{i}'")
                    print("why")
                    db.addBlacklistChar(i)
        # iterate twice, because some emojis are unicode en get added above, and custom emojis get added now
        print("checked unicode, now custom")
        for i in self.bot.emojis:
            if str(i) in ctx.message.content:
                await ctx.message.add_reaction(i)
