import sqlite3


class database:
    def __init__(self):
        self.connection = None
        self.c = None

    def connect(self):
        # doesn't really do much but maybe useful
        self.connection = sqlite3.connect("brabo.db")
        self.c = self.connection.cursor()

    def disconnect(self):
        # doesn't do much but maybe usefull
        self.connection.close()
        self.c = None
        self.connection = None

    def insertStatsLine(self, message):
        # self.connect()
        return str(
            [
                message.author.name,
                message.channel.name,
                message.guild.name if message.guild else None,
            ]
        )

        # self.c.execute()
        # self.disconnect()

    def inserthm(self, hmgame):
        self.connect()

        # debug things
        print(hmgame)

        # update score of the winner
        print("winner:")
        print(str(hmgame.winner))
        self.c.execute(
            """SELECT score FROM hangmanScores WHERE username = :user""",
            {"user": str(hmgame.winner.id)},
        )
        out = self.c.fetchone()
        print("out = " + str(out))
        if type(out) is type(None):
            score = None
        else:
            score = out[0]
        print("score:")
        print(score)
        if type(score) is int:
            score = score + 1
            self.c.execute(
                """UPDATE hangmanScores SET score = :score WHERE username = :user""",
                {"user": str(hmgame.winner.id), "score": score},
            )
        else:
            score = 1
            self.c.execute(
                """INSERT INTO hangmanScores VALUES (:user, :score)""",
                {"user": str(hmgame.winner.id), "score": score},
            )

        self.connection.commit()

        # build players string
        playersStr = ""
        for user in hmgame.participants:
            playersStr = playersStr + ", " + str(user.id)

        # build "used" string
        used = ""
        for letter in hmgame.usedletters:
            used = used + ", " + letter

        # push game to database for storage
        self.c.execute(
            """INSERT INTO hangmanGames VALUES (:word, :used, :wrong, :winner, :participants, :startdate, :enddate)""",
            {
                "word": hmgame.word,
                "used": used,
                "wrong": hmgame.wrong,
                "winner": str(hmgame.winner.id),
                "participants": playersStr,
                "startdate": hmgame.startDate,
                "enddate": hmgame.endDate,
            },
        )
        self.connection.commit()
        self.disconnect()

    def selectScore(self, username):
        self.connect()
        self.c.execute(
            """SELECT score FROM hangmanScores WHERE username = :user""",
            {"user": username},
        )
        score = self.c.fetchone()[0]
        self.disconnect()
        if type(score) is int:
            return score
        else:
            return -1

    def selectTopScores(self):
        self.connect()
        self.c.execute(
            """SELECT score, username FROM hangmanScores ORDER BY score DESC LIMIT 4"""
        )
        results = self.c.fetchall()
        self.disconnect()
        return results

    # checks whehthehr $char is in the blacklist for characters
    def isBlacklistChar(self, char):
        print("checking blacklisted chars for '" + char + "'")
        self.connect()

        self.c.execute(
            """SELECT character FROM emojiBlacklist WHERE character = :char""",
            {"char": char},
        )
        if self.c.fetchone() == None:
            # if none are returned it's not in the blacklist
            self.disconnect()
            return False
        else:
            self.disconnect()
            return True

    # adds character $char to the blacklist
    def addBlacklistChar(self, char):
        print("adding to blacklist: '" + char + "'")
        self.connect()
        self.c.execute("""INSERT INTO emojiBlacklist VALUES (:char)""", {"char": char})
        self.connection.commit()
        self.disconnect()
        return
