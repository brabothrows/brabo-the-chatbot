import asyncio
import requests
import time
from discord.ext import commands
import sys

class AdminCommands(commands.Cog):
	def __init__(self, bot):
		self.bot = bot
		#				Ratty				 Fantom               Elmusfire
		self.dev_ids = ['79884312748498944', '79622319546310656', '83556723851264000']
		self.start_time = time.time()

	@commands.command(no_pm=False)
	async def test(self, ctx, msg = None):
		"""replies with what msg looks like to the bot"""
		print("test called, ctx looks like : \n" + str(dir(ctx.message)) +
				#"\nmsg2 = " + str() +
			  "\ntype:" + str(type(msg)) +
			  "\nsender:" + str(ctx.message.author.id) +
			  "\nmsg:" + str(msg) +
			  "\nctx message:" + str(ctx.message.content) +
			  "\nctx clean message:" + str(ctx.message.clean_content) +
			  "\n:"
		)
		try:
			print(msg.reactions)
		except:
			print("nvm")
			print("dir:", dir(msg))
		#for part in ctx.message.content.split(":")
		#	await self.bot.add_reaction(ctx.message, emoji)
		await ctx.send('this looks like `' + str(ctx.message.clean_content) + '`')
		await ctx.send('this looks like `' + str(ctx.message.content) + '`')

	@commands.command(no_pm=False)
	async def testfull(self, ctx, *msg):
		"""replies with what msg looks like to the bot"""
		if msg is not None:
			await ctx.send('this looks like `' + str(msg) + '`')
		else:
			await ctx.send("I didn't get anything")

	@commands.command(no_pm=False)
	async def uptime(self, ctx):
		"""replies with what msg looks like to the bot"""
		waketime = time.time() - self.start_time
		wakeword = "seconds"
		if waketime / 60 > 2:
			waketime =  waketime // 60
			wakeword = "minutes"
			if waketime / 60 > 2:
				waketime = waketime // 60
				wakeword = "hours"
				if waketime / 24 > 2:
					waketime = waketime // 24
					wakeword = "days"
		blurb = "" # todo: add random things like "beat that, humans" and "I could use a break" about .1% of the time
		await ctx.send("I've been awake for " + str(int(waketime)) + " " + wakeword + blurb + ".")

	@commands.command(no_pm=False)
	async def plseval(self, ctx, msg):
		"""evals the expression (admin only)"""
		if ctx.message.author.id in self.dev_ids:
			await ctx.send("evaluating " + str(ctx.message))
			await ctx.send(str(eval(ctx.message)))
			await ctx.send("done")
		else:
			await ctx.send("You're not allowed to do that!")

	@commands.command(no_pm=False)
	async def ip(self, ctx):
		"""Prints IP (admin only)"""
		if ctx.message.author.id in self.dev_ids:
			await ctx.send(requests.get('http://ip.42.pl/raw').text)
		else:
			await ctx.send("You're not allowed to do that!")


	@commands.command(no_pm=False)
	async def reboot(self, ctx):
		"""Reboots the bot (admin only)"""
		caller = ctx.message.author.id
		if str(caller) in self.dev_ids:
			await ctx.send("Rebooting...")
			sys.exit(0) # lol
		else:
			await ctx.send(f"{caller} is not in the sudoers file. This incident will be reported.")

